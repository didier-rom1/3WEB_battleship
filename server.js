var express    = require('express');
var port       = process.env.PORT || 3000;
var morgan     = require('morgan');
var mongoose   = require('mongoose');
var bodyParser = require('body-parser');
var router     = express.Router();
var appRoutes  = require('./app/routes/api')(router);
var path       = require('path');
var http       = require('http');
var app        = express();

var server     = http.createServer(app).listen(port, function(){
  console.log('Express server listening on port ' + port);
});

var io         = require('socket.io').listen(server);

connections = [];


//app.listen(port,function(){
//  console.log("Server running on " + port);
//});

app.use(morgan('dev'));
app.use(bodyParser.json()); //for parsing application/json
app.use(bodyParser.urlencoded({extended:true})); //for parsing application/x-www-form-urlencoded
app.use(express.static(__dirname + '/public'));
app.use('/api', appRoutes);

mongoose.connect('mongodb://localhost:27017/battleship-db', function(err){
  if(err){
    console.log('Not connected to the database : ' + err);
  } else {
    console.log('Successfully connected to the MongoDB');
  }
});


//Socket on connect
io.sockets.on('connection', function (socket) {
  console.log('client connected');
});



app.get('*', function(req, res){
  res.sendFile(path.join(__dirname + '/public/app/views/index.html'));
});
