var app = angular.module('battleshipRoutes', ['ngRoute'])

.config(function($routeProvider, $locationProvider){
  $routeProvider

  .when('/',{
      templateUrl: 'app/views/pages/home.html'
  })

  .when('/about',{
      templateUrl: 'app/views/pages/about.html'
  })

  .when('/register',{
      templateUrl: 'app/views/pages/users/register.html',
      controller: 'regCtrl',
      controllerAs: 'register',
      authenticated: false
  })

  .when('/login',{
      templateUrl: 'app/views/pages/users/login.html',
      authenticated: false
  })

  .when('/logout',{
      templateUrl: 'app/views/pages/users/logout.html',
      authenticated: true
  })

  .when('/profile',{
      templateUrl: 'app/views/pages/users/profile.html',
      authenticated: true
  })

  .when('/chat',{
      templateUrl: 'app/views/pages/lobby/chat.html',
      controller: 'chatCtrl',
      controllerAs: 'chat',
      authenticated: true
  })

  .when('/game',{
      templateUrl: 'app/views/pages/game/area.html',
      controller: 'gameCtrl',
      controllerAs: 'game',
      authenticated: true
  })


  .when('/resetusername',{
      templateUrl: 'app/views/pages/users/reset/username.html',
      controller: 'usernameCtrl',
      controllerAs: 'username',
      authenticated: false
  })

  .when('/resetpassword',{
      templateUrl: 'app/views/pages/users/reset/password.html',
      controller: 'passwordCtrl',
      controllerAs: 'password',
      authenticated: false
  })

  .otherwise({redirectTo: '/'} );

  $locationProvider.html5Mode({
    enable: true,
    requireBase: false
  });
});

app.run(['$rootScope', 'Login','$location', function($rootScope, Login, $location){
  $rootScope.$on('$routeChangeStart', function(event, next, current){

    if (next.$$route.authenticated == true){
      if(!Login.isLoggedIn()){
        event.preventDefault();
        $location.path('/');
      };

    } else if (next.$$route.authenticated == false){
      if(Login.isLoggedIn()){
        event.preventDefault();
        $location.path('/profile');
      };

    }

  });

}]);
