angular.module('chatController', ['messageServices', 'loginServices'])

.controller('chatCtrl', function (Login, $http, $scope, Message) {
  var app = this;

  app.username = Login.getUser().then(function(data){
    app.username = data.data.username;
    app.loadme =true;
  });

	app.message = '';

	var socket = io.connect();

	//recieve new messages from chat
	socket.on('receiveMessage', function(data) {
		$scope.messages.unshift(data);
		$scope.$apply();
	});

//load previous messages from DB
Message.getMessages().then(function(data){
  app.messages = data.data;
});

	//send a message to the server
	$scope.sendMessage = function() {
		if (!$scope.message == '') {
			var messageSchema = {
				'username' : app.username,
				'message' : $scope.message
			};

			messageFactory.postMessage(messageSchema, function (result, error) {
				if (error) {
					window.alert('Error saving to DB');
					return;
				}
				$scope.message = '';
			});
		}
	};
});
