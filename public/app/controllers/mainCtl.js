angular.module('mainController', ['loginServices'])

.controller('mainCtrl', function(Login, $timeout, $location, $rootScope){
  var app = this;

  app.loadme =false;

  $rootScope.$on('$routeChangeStart', function(){
    if(Login.isLoggedIn()){
      app.isLoggedIn = true;
      Login.getUser().then(function(data){
        app.username = data.data.username;
        app.useremail = data.data.email;
        app.loadme =true;
      });
    } else {
        app.isLoggedIn = false;
        app.username = '';
        app.loadme =true;
    }

  });

  this.doLogin = function(loginData){
    app.loading = true;
    app.errorMessage = false;
    app.successMessage = false;

    Login.login(app.loginData).then(function(data) {

      if(data.data.success){

        app.loading = false;
        //Creation du message de success
        app.successMessage = data.data.message + "... Redirection";
        //Redirection sur la page home
        $timeout(function() {
          $location.path('/');
          app.loginData='';
          app.successMessage = false;
        }, 2000);


      } else {
          //Creation du message de erreur
          app.loading = false;
          app.errorMessage = data.data.message;
      }
    });
  };

  this.logout = function(){
    Login.logout();
    $location.path('/logout');

    $timeout(function() {
    $location.path('/');
    }, 2000);

  };

});
