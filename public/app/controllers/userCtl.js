angular.module('userControllers', ['userServices'])

.controller('regCtrl',function($http, $location, $timeout, User){

    var app = this;

    this.regUser = function(regData){
      app.loading = true;
      app.errorMessage = false;
      app.successMessage = false;

      User.create(app.regData).then(function(data) {
        console.log(data.data.success);
        console.log(data.data.message);
        if(data.data.success){

          app.loading = false;
          //Creation du message de success
          app.successMessage = data.data.message + "... Redirection";
          //Redirection sur la page home
          $timeout(function() {
            $location.path('/');
          }, 2000);


        } else {
          //Creation du message de erreur
          app.loading = false;
          app.errorMessage = data.data.message;
        }

      });
    };
});
