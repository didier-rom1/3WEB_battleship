angular.module('messageServices', [])

.factory('Message', function($http){
    messageFactory = {};

    //Message.create(msgData)
    messageFactory.postMessage = function(msgData) {
      return $http.post('/api/message', msgData);
    }


    //Message.getMessage();
    messageFactory.getMessages = function() {
      return $http.get('/api/message');
    }

return messageFactory;
});
