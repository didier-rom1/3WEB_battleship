angular.module('loginServices', [])

.factory('Login', function($http, LoginToken){
    loginFactory = {};

    //User.create(regData)
    loginFactory.login = function(loginData) {
      return $http.post('/api/login', loginData).then(function(data) {
        LoginToken.setToken(data.data.token);
        return(data);
      });
    };

    //Login.isLoggedIn();
    loginFactory.isLoggedIn = function() {
      if (LoginToken.getToken()) {
        return true;
      } else {
        return false;
      }

    };

    //Login.getUser();
    loginFactory.getUser = function(){
      if (LoginToken.getToken()) {
        return $http.post('/api/me');
      } else {
        $q.reject({message: 'L\'utilisateur n\'a pas de token! ' });
      }
    };

    //Login.logout();
    loginFactory.logout = function(){
      LoginToken.setToken();
    };

    return loginFactory;
})

.factory('LoginToken',function($window){
  var loginTokenFactory = {};

  //LoginToken.setToken(token);
  loginTokenFactory.setToken = function(token) {
    if(token){
      $window.localStorage.setItem('token', token);
    } else {
      $window.localStorage.removeItem('token');
    }

  };
  //LoginToken.getToken();
  loginTokenFactory.getToken = function(){
    return $window.localStorage.getItem('token');
  };

  return loginTokenFactory;
})

.factory('LoginInterceptors', function(LoginToken){
  var loginInterceptorsFactory ={};

  loginInterceptorsFactory.request = function(config) {

    var token = LoginToken.getToken();

    if (token) {
      config.headers['x-access-token'] = token;
    } else {

    }

    return config;
  };

  return loginInterceptorsFactory;
});
