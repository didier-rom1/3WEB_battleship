angular.module('battleshipApp', ['battleshipRoutes', 'userControllers', 'userServices', 'messageServices','mainController', 'loginServices', 'emailController', 'chatController', 'gameController'])
.config(function($httpProvider){
  $httpProvider.interceptors.push('LoginInterceptors');
})
.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keypress", function (event) {
            if(event.which === 13) {
                scope.$apply(function (){
                    scope.$eval(attrs.ngEnter);
                });
                event.preventDefault();
            }
        });
    };
});
