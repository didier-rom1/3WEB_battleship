var mongoose = require('mongoose');
var Schema   = mongoose.Schema;


//MongoDB Schemas
var messageSchema = new mongoose.Schema({
	username: String,
	message: String
});

//var Message = mongoose.model('Message', messageSchema);

module.exports = mongoose.model('Message', messageSchema);
