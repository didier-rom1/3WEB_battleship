var User      = require('../models/user');
var Message   = require('../models/message');
var jwt       = require('jsonwebtoken');
var secret    = 'secrettoken';
var http       = require('http');
var io         = require('socket.io')(http);

module.exports = function(router) {

  //User registration route
  //http://localhost:3000/api/users
  router.post('/users',function(req,res){
    var user = new User();
    user.name = req.body.name;
    user.username = req.body.username;
    user.password = req.body.password;
    user.email = req.body.email;
    if (req.body.name == null || req.body.name == '' || req.body.username == null || req.body.username == '' || req.body.password == null || req.body.password == '' || req.body.email == null || req.body.email == '' ) {
      res.json({success:false, message: 'veuillez remplir tous les champs requis'});
    } else {
      user.save(function(err){
        if (err){
          if (err.errors.name){
            res.json({success:false, message: err.errors.name.message});
          } else if (err.errors.email) {
              res.json({success:false, message: err.errors.email.message});
          }
        } else {
          res.json({success:true, message: 'Vous êtes correctement enregistrés!'});
        }
      });
    }
  });

  //Login registration route
  //http://localhost:3000/api/login
  router.post('/login',function(req,res){
    User.findOne({ username: req.body.username }).select('username password email').exec(function(err, user){
      if (err) throw err;

      if (!user){
        res.json({success: false, message :'Nous ne pouvons pas vous identifier!' });
      } else if (user) {
        if (req.body.password) {
          var validPassword = user.comparePassword(req.body.password);
        } else {
          res.json({success: false, message :'entrez un mot de passe!'});
        }

        if (!validPassword) {
          res.json({success: false, message :'Mot de passe incorrect!'});
        } else {
          var token = jwt.sign({ username: user.username, email: user.email}, secret, {expiresIn: '24h'});
          res.json({success: true, message :'authentification ok!',token: token});
        }
      }
    });
  });

router.get('/resetusername',function(req, res){
  User.findOne({ username: req.body.email })
});

router.get('/resetpassword',function(req, res){
});

//test chat
  //Message registration route
  //http://localhost:3000/api/message
router.post('/message', function (req, res) {

	var message = new Message();
		  message.username= req.body.username;
      message.message = req.body.message;

    message.save(function (err, saved) {
    	if (err) {
    		res.send(400);
    		return console.log('error saving to db');
    	}
    	res.send(saved);
    	io.sockets.emit('receiveMessage', saved);
    })
});

router.get('/message', function (req, res) {
	Message.find(function (err, allMessages) {
  	if (err) {
  		return res.send(err);
  	};
  	res.send(allMessages);
  })
});

  router.use(function(req, res, next){

  var token = req.body.token || req.body.query || req.headers['x-access-token'];

  if (token){
    // Verify token
    jwt.verify(token, secret, function(err, decoded){
      if (err) {
        res.json({ success: false, message:'Le token est invalide!'});
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    res.json({ success: false, message:'pas de token défini!'});
  }

  });
  router.post('/me', function(req,res){
    res.send(req.decoded);
  });

  return router;
}
